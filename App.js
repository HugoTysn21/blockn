import {StyleSheet, Text, View} from 'react-native';
import {Provider} from "react-redux";
import store from "./app/Store/store";
import {TabNavigator} from "./app/navigation/navigation";
import {useEffect} from "react";
import SplashScreen from 'react-native-splash-screen'

export default function App() {

    return (
            <Provider store={store}>
                <TabNavigator/>
            </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
