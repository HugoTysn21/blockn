import {fetchIcons, fetchSpecificAssets, updatePrice, updateVolume} from "../Store/actions";
import {Text, View, TouchableOpacity, FlatList, StyleSheet} from "react-native";
import {StatusBar} from "expo-status-bar";
import {connect} from "react-redux";
import React from "react";
import AssetsListItem from "./AssetsListItem";
import {API_KEY_COIN_CAP} from "../Services/apiKey";
import {SafeAreaView} from "react-navigation";

class AssetsComponent extends React.Component {


    componentDidMount() {

        this.props.dispatch(fetchIcons())
        this.props.dispatch(fetchSpecificAssets())

        const ws = new WebSocket('wss://ws.coincap.io/prices?assets=the-graph,bitcoin,ethereum,tether,binance-coin,usd-coin,terra-luna,xrp,cardano,solana,avalanche,polkadot,binance-usd,dogecoin,terrausd,shiba-inu,polygon,wrapped-bitcoin,multi-collateral-dai,crypto-com-coin,litecoin,cosmos,near-protocol,chainlink,tron,uniswap,ftx-token,unus-sed-leo,bitcoin-cash,steth,algorand,decentraland,stellar,bitcoin-bep2,ethereum-classic,internet-computer,fantom,the-sandbox,filecoin,elrond-egld,monero,hedera-hashgraph,vechain,klaytn,frax,theta,waves,axie-infinity,tezos,helium,iota,flow,eos,zcash,maker,pancakeswap,aave,thorchain,trustnote,gala,bitcoin-sv,ecash,harmony,trueusd,kucoin-token,neo,huobi-token,stacks,quant,celo,nexo,enjin-coin,chiliz,arweave,kusama,dash,kadena,okb,amp,defichain,basic-attention-token,paxos-standard,loopring,curve-dao-token,convex-finance,nem,theta-fuel,oasis-network,symbol,celsius,decred,secret,ecomi,mina,yearn-finance,holo,compound,iotex,xinfin-network,omg');

        ws.onopen = () => {
            //CALL TO SUBSCRIPTION
            //ws.send(JSON.stringify(initMessageQuote))
            //ws.send(JSON.stringify(initMessageVolume))
        }

        ws.onmessage = (socketData) => {

            const data = JSON.parse(socketData.data)

            const payload = {
                id: null,
                price: null
            }

            Object.keys(data).forEach(key => {
                payload.id = key
                payload.price = data[key]
            })

            this.props.dispatch(updatePrice(payload))
        };

        /*setInterval(() => {
            if (payload === null) {
            }
            lastPayload = this.payload
            this.props.dispatch(updatePrice(lastPayload))
        }, delay)*/

        ws.onerror = (e) => {
            // an error occurred
            console.log(e.message);
        };

        ws.onclose = (e) => {
            // connection closed
            console.log(e.code, e.reason);
        };
    }

    render() {

        const {assets, priceAssets, loading, iconAssets} = this.props
        // GET PRICE ID FOR ASSET ID
        const renderItem = ({item}) => {
            const assetPrice = priceAssets.find(x => {
                if (x.id === item.id) {
                    return x.price
                }
            })
            const icon = iconAssets.find(x => x.asset_id === item.symbol);


            return <AssetsListItem navigation={this.props.navigation} icon={icon} assetsItem={item} priceForItem={assetPrice}/>
        }

        if (!loading) {
            return (
                <SafeAreaView>
                    <TouchableOpacity style={styles.list}>
                        <FlatList
                            data={assets.data}
                            renderItem={({item}) => renderItem({item})}
                            keyExtractor={item => item.id}
                        />
                        <StatusBar style="auto"/>
                    </TouchableOpacity>
                </SafeAreaView>
            )
        } else {
            return (
                <View>
                    <Text title={"loading..."}/>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    list: {
        backgroundColor: 'white'
    },
});

const mapStateToProps = (state) => {
    return {
        assets: state.assetReducer.assetsData,
        assetsLoading: state.assetsLoading,
        iconAssets: state.iconReducer.iconAssets,
        iconLoading: state.iconLoading,
        priceAssets: state.priceReducer.priceAssets,
    }
}
/*const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => { dispatch(action) }
    }
}*/
export default connect(mapStateToProps)(AssetsComponent)

