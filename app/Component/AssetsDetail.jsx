import React from "react";
import {Image, Linking, StyleSheet, Text, View, ScrollView} from "react-native";
import {connect} from "react-redux";
import {fetchCandleAssets} from "../Store/actions";
import moment from "moment";
import {SafeAreaView} from "react-navigation";


class AssetsDetail extends React.Component {

    componentDidMount() {

        // start = now en timestamp unix epoch end = period 3M 1 an etc
        this.props.dispatch(fetchCandleAssets("binance", "d1", "bitcoin", "tether", moment().subtract(3, 'months'), moment()))
    }

    render() {

        const imageId = this.props.route.params.imageId
        let {name, priceUsd, id, explorer, marketCapUsd, maxSupply, rank, supply, symbol} = this.props.route.params.item
        const price = this.props.route.params.priceForItem
        const volume = this.props.route.params.volume
        const start = moment()
        const end = moment().subtract(3, 'months');

        const numFormatter = (vol) => {
            const num = vol.split(".")[0]
            if (num > 999 && num < 1000000) {
                return (num / 1000).toFixed(1) + 'K'; // convert to K for number from > 1000 < 1 million
            } else if (num > 1000000 && num < 1000000000) {
                return (num / 1000000).toFixed(1) + 'M'; // convert to M for number from > 1 million
            } else if (num > 1000000000) {
                return (num / 1000000000).toFixed(2) + 'B'; // convert to M for number from > 1 million
            } else if (num < 900) {
                return num; // if value < 1000, nothing to do
            }
        }

        const calcSupply = () => {
            if (maxSupply == null) {
                supply = parseFloat(supply).toFixed(0)
                return numFormatter(supply) + " " + symbol
            } else {
                supply = parseFloat(supply).toFixed(0)
                maxSupply = parseFloat(maxSupply).toFixed(0)
                let percent = supply / maxSupply * 100 + " " + symbol
                return parseFloat(percent).toFixed(2) + "% " + symbol
            }
        }

        console.log(this.props.route.params.item)
        return (
            <SafeAreaView style={styles.safeContainer}>
                <ScrollView style={styles.wrapContainer}>
                    <View style={styles.itemContainer}>
                        <View style={styles.itemImageContainer}>
                            <Image source={{
                                uri: `https://cryptoicon-api.vercel.app/api/icon/${imageId}`
                            }} style={styles.itemImage}/>
                        </View>
                        <View style={styles.itemTitleContainer}>
                            <Text style={styles.nameContainer}>{symbol}</Text>
                            <Text style={styles.priceContainer}>{price}$</Text>
                        </View>
                        <View style={styles.itemRankContainer}>
                            <Text style={styles.rankItem}>Rank</Text>
                            <Text style={styles.rankItem}>#{rank}</Text>
                        </View>
                    </View>
                    <View style={styles.chartsContainer}>

                    </View>
                    <View style={styles.infoContainer}>
                        <Text style={styles.itemInfoContainer}>Project Name : {name}</Text>
                        <Text style={styles.itemInfoContainer}>Market Capitalization : {numFormatter(marketCapUsd)}$</Text>
                        <Text style={styles.itemInfoContainer}>Circulating supply : {calcSupply()}</Text>
                        <Text style={styles.linkContainer} onPress={() => Linking.openURL(explorer)}>
                            Go to {name} explorer !</Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({

    safeContainer: {
        backgroundColor: 'darkblue',
    },
    wrapContainer: {
        display: "flex",
        flexDirection: "column",
        backgroundColor: 'white',
        padding: 10,
        width: '100%',
        height: '100%',
    },
    itemContainer: {
        display: "flex",
        flexDirection: "row",
        width: '100%',
        marginTop: 10,
    },
    chartsContainer: {
        borderStyle: "solid",
        borderWidth: 1,
        marginTop: 30,
        width: '100%',
        height: '100%'
    },
    infoContainer: {
        borderStyle: "solid",
        marginTop: 20,
        display: "flex",
        flexDirection: "column"
    },
    itemImageContainer: {
        flex: 1,
    },
    itemImage:{
        width: 100,
        height: 100,
        alignSelf: "center",
        borderRadius: 80,
    },
    itemRankContainer: {
        flex: 1,
        alignSelf: "center",
    },
    rankItem: {
        fontSize: 27,
        fontWeight: "500",
        alignSelf: "center",
    },
    itemTitleContainer: {
        display: "flex",
        margin: 10,
        flex: 1,
    },
    nameContainer: {
        fontSize: 20,
        fontWeight: "500",
        marginBottom: 10,
        marginTop: 10,
        marginLeft: 10,
    },
    priceContainer: {
        fontSize: 20,
        fontWeight: "500",
        marginBottom: 10,
        marginLeft: 10,
    },
    itemInfoContainer: {
        fontSize: 16,
        fontWeight: "500",
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
    },
    linkContainer: {
        fontSize: 16,
        fontWeight: "500",
        marginBottom: 10,
        marginTop: 10,
        marginLeft: 10,
        color: 'blue'
    }
});


const mapStateToProps = (state) => {
    return {
        assets: state.assetsData,
    }
}

export default connect(mapStateToProps)(AssetsDetail)
