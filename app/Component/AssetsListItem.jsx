import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import React from "react";

class AssetsListItem extends React.Component {

    render() {

        const {assetsItem, priceForItem, icon} = this.props
        const {name, priceUsd, changePercent24Hr, volumeUsd24Hr, symbol, id} = assetsItem
        const change24hrs = parseFloat(changePercent24Hr).toFixed(2)

        console.log("MY ICON PROPS", icon)

        const imageIcon = () => {
            if (icon === undefined) {
                return <Image source={require("../../assets/ada.png")} style={{
                    width: 50,
                    height: 50,
                    borderRadius: 80,
                }}
                >
                </Image>
            } else {
                return <Image source={{
                    uri: icon.url
                }} style={{
                    width: 50,
                    height: 50,
                    borderRadius: 80,
                }}
                >
                </Image>
            }
        }

        function numFormatter(vol) {
            const num = vol.split(".")[0]
            if (num > 999 && num < 1000000) {
                return (num / 1000).toFixed(1) + 'K'; // convert to K for number from > 1000 < 1 million
            } else if (num > 1000000 && num < 1000000000) {
                return (num / 1000000).toFixed(1) + 'M'; // convert to M for number from > 1 million
            } else if (num > 1000000000) {
                return (num / 1000000000).toFixed(2) + 'B'; // convert to M for number from > 1 million
            } else if (num < 900) {
                return num; // if value < 1000, nothing to do
            }
        }

        const change24h = () => {
            if (change24hrs < 0) {
                return <View style={{backgroundColor: "red", borderRadius: 5, width:75}}>
                    <Text style={{fontSize: 14 , color: "white", margin: 7, fontWeight: "600", alignSelf: "center"}}>{change24hrs} %</Text>
                </View>
            } else if (change24hrs > 0) {
                return <View style={{backgroundColor: "green", borderRadius: 5, width: 75}}>
                    <Text style={{fontSize: 14 , color: "white", margin: 7, fontWeight: "600", alignSelf: "center"}}>{change24hrs} %</Text>
                </View>
            } else {
                return <View style={{backgroundColor: "black", borderRadius: 5, width: 75}}>
                    <Text style={{fontSize: 14 , color: "white", margin: 7, fontWeight: "600", alignSelf: "center"}}>{change24hrs} %</Text>
                </View>
            }
        }

        const pricing = () => {
            if (priceForItem === undefined) {
                const stringPrice = priceUsd.toString()
                if (stringPrice.startsWith("0.")) {
                    return parseFloat(priceUsd).toFixed(3);
                } else if (stringPrice.startsWith("0.0")) {
                    return parseFloat(priceUsd).toFixed(4);
                } else {
                    return parseFloat(priceUsd).toFixed(2);
                }
            } else {

                const stringPrice = priceForItem.price.toString()
                if (stringPrice.startsWith("0.")) {
                    return parseFloat(priceForItem.price).toFixed(3);
                } else if (stringPrice.startsWith("0.0")) {
                    return parseFloat(priceForItem.price).toFixed(4);
                } else {
                    return parseFloat(priceForItem.price).toFixed(2);
                }
            }
        }
        const imageId = symbol.toLowerCase()

        return (
            <View>
                <TouchableOpacity
                    onPress={() =>
                        this.props.navigation.navigate('AssetsDetail', {
                            item: this.props.assetsItem,
                            imageId: imageId,
                            priceForItem: pricing(),
                            volume: numFormatter(volumeUsd24Hr)
                        })}>
                    <View style={styles.itemContainer}>
                        <View style={styles.imageContainer}>
                            {imageIcon()}
                        </View>
                        <View style={styles.textContainer}>
                            <View style={styles.textContainerPrice}>
                                <Text style={styles.textName}>{symbol}/USDT</Text>
                                <Text style={styles.price}>${pricing()}</Text>
                            </View>
                            <View>
                                <Text style={styles.volumeContainer}>24H Vol : {numFormatter(volumeUsd24Hr)}$</Text>
                            </View>
                        </View>
                        <View style={styles.percent}>
                            {change24h()}
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    itemContainer: {
        flexDirection: "row",
        justifyContent: "flex-start",
    },
    imageContainer: {
        alignSelf: "center",
        margin: 10,
    },
    textContainer: {
        alignItems: "flex-start",
        flexDirection: "column",
        margin: 10,
        width: 200,


    },
    volumeContainer: {
        marginLeft: 10,
        fontWeight: "500",
        marginBottom: 10,

    },
    textContainerPrice: {
        flexDirection: "row",
    },
    textName: {
        fontSize: 14,
        fontWeight: "500",
        marginBottom: 10,
        marginTop: 10,
        marginLeft: 10,
        alignSelf: "center",

    },
    percent: {
        alignSelf: "center"
    },
    price: {
        fontSize: 15,
        fontWeight: "600",
        maxWidth: 100,
        margin: 10,

    }
});


const mapStateToProps = (state) => {
    return {
        text: state.text,
        priceAssets: state.priceReducer.priceAssets,
        iconAssets: state.iconReducer.iconAssets
    }
}

export default connect(mapStateToProps)(AssetsListItem)
