import React from "react";
import {Button, Text, TouchableOpacity, View} from "react-native";
import {StatusBar} from "expo-status-bar";
import {connect} from "react-redux";
import AssetsComponent from "./AssetsComponent";
import {fetchSpecificAssets} from "../Store/actions";


class HomeComponent extends React.Component {

    render() {

        return (
            <View>
                <AssetsComponent navigation={this.props.navigation}/>
            </View>
        )}
}

const mapStateToProps = (state) => {
    return {
        state
    }
}
export default connect(mapStateToProps)(HomeComponent)
