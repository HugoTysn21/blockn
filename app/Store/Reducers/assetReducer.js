const initialState = {
    assetsLoading: false,
    assetsError: "",
    assetsData: [],
}

export default function assetReducer(state = initialState, action) {

    let nextState;
    switch (action.type) {
        case 'GET_ASSETS_REQUEST' :
            nextState = {
                ...state,
                assetsLoading: true
            }
            return nextState
        case 'GET_ASSETS_SUCCESS' :
            nextState = {
                ...state,
                assetsLoading: false,
                assetsData: action.payload
            }
            return nextState
        case 'GET_ASSETS_FAILURE' :
            nextState = {
                ...state,
                assetsLoading: false,
                assetsError: action.payload
            }
            return nextState
        default :
            return state
    }
}