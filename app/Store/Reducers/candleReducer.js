const initialState = {
   can
}

export default function assetReducer(state = initialState, action) {

    let nextState;
    switch (action.type) {
        case 'GET_CANDLE_REQUEST' :
            nextState = {
                ...state,
                assetsLoading: true
            }
            return nextState
        case 'GET_CANDLE_SUCCESS' :
            nextState = {
                ...state,
                assetsLoading: false,
                assetsData: action.payload
            }
            return nextState
        case 'GET_CANDLE_FAILURE' :
            nextState = {
                ...state,
                assetsLoading: false,
                assetsError: action.payload
            }
            return nextState
        default :
            return state
    }
}