const initialState = {
    iconLoading: false,
    iconError: "",
    iconAssets: [],
}

export default function iconReducer(state = initialState, action) {

    let nextState;
    switch (action.type) {
        case 'GET_ICON_REQUEST' :
            nextState = {
                ...state,
                iconLoading: true
            }
            return nextState
        case 'GET_ICON_SUCCESS' :
            nextState = {
                ...state,
                iconLoading: false,
                iconAssets: action.payload
            }
            return nextState
        case 'GET_ICON_FAILURE' :
            nextState = {
                ...state,
                iconLoading: false,
                iconError: action.payload
            }
            return nextState
        default :
            return state
    }
}