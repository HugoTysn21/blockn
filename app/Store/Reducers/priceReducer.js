const initialState = {
    priceAssets: [],
    /*HRSvOL: [],
    DAYvOL: [],
    MONTHvOL: []*/

}

export default function priceReducer(state = initialState, action) {

    let nextState;
    switch (action.type) {
        case 'UPDATE_PRICE' :

            const priceIndex = state.priceAssets.findIndex(x => x.id === action.payload.id)
            // si deja dans priceAssets
            const nextPriceAsset = [...state.priceAssets]
            if (priceIndex !== -1) {
                nextPriceAsset[priceIndex] = action.payload
                nextState = {
                    ...state,
                    priceAssets: nextPriceAsset
                }
            } else {
                nextState = {
                    ...state,
                    priceAssets: [...state.priceAssets, action.payload]
                }
            }
            return nextState
        /*case 'UPDATE_VOLUME':

            console.log("BEFORE", state.HRSvOL)

            //every call = replace

            if (action.payload.period === '1HRS'){
                nextState = {
                    ...state,
                    HRSvOL: action.payload.volume_by_symbol
                }
            } else if (action.payload.period === '1DAY'){
                nextState = {
                    ...state,
                    DAYvOL: action.payload.volume_by_symbol
                }
            } else if (action.payload.period === '1MTH') {
                nextState = {
                    ...state,
                    MONTHvOL: action.payload.volume_by_symbol
                }
            }
            return nextState*/
        default :
            return state
    }
}