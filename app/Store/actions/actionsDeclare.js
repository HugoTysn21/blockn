import * as actions from "./actionsConstant";

export const updatePrice = (price) => {
    return {
        type: actions.UPDATE_PRICE,
        payload: price
    }
}

export const getCandleSuccess = (candle) => {
    return {
        type: actions.GET_CANDLE_SUCCESS,
        payload: candle
    }
}

export const getCandleRequest = () => {
    return {
        type: actions.GET_CANDLE_REQUEST
    }
}

export const getCandleFailure = (error) => {
    return {
        type: actions.GET_CANDLE_FAILURE,
        payload: error
    }
}

export const updateVolume = (volume) => {
    return {
        type: actions.UPDATE_VOLUME,
        payload: volume
    }
}

export const getAssetsRequest = () => {
    return {
        type: actions.GET_ASSETS_REQUEST
    }
}

export const getAssetsSuccess = (assets) => {
    return {
        type: actions.GET_ASSETS_SUCCESS,
        payload: assets
    }
}

export const getAssetsFailure = (error) => {
    return {
        type: actions.GET_ASSETS_FAILURE,
        payload: error
    }
}

export const getIconsRequest = () => {
    return {
        type: actions.GET_ICON_REQUEST
    }
}

export const getIconsSuccess = (icons) => {
    return {
        type: actions.GET_ICON_SUCCESS,
        payload: icons
    }
}

export const getIconsFailure = (error) => {
    return {
        type: actions.GET_ICON_FAILURE,
        payload: error
    }
}
