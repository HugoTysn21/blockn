import {API_KEY_COIN_CAP, API_KEY_COIN_IO} from "../../Services/apiKey";
import * as action from './actionsDeclare'

export const updatePrice = (data) => {
    return (dispatch) => {
        dispatch(action.updatePrice(data))
    }
}

export const updateVolume = (data) => {
    return (dispatch) => {
        dispatch(action.updateVolume(data))
    }
}

export const fetchIcons = () => {
    return (dispatch) => {
        dispatch(action.getIconsRequest())
        fetch(
            "https://rest.coinapi.io/v1/assets/icons/64", {
                method: 'GET',
                headers: {
                    'X-CoinAPI-Key': API_KEY_COIN_IO,
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        ).then(response => response.json())
            .then(data => {
                const assets = data
                dispatch(action.getIconsSuccess(data))
            })
            .catch(error => {
                const errorMessage = error.message
                dispatch(action.getIconsFailure(errorMessage))
            })
    }
}

// NEW API PREVIOUSLY USE COIN API.IO
export const fetchSpecificAssets = () => {
    return (dispatch) => {
        dispatch(action.getAssetsRequest())
        fetch(
            "https://api.coincap.io/v2/assets", {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${API_KEY_COIN_CAP}`,
                    'Accept-Encoding': 'gzip',
                    'Content-Type': 'application/json'
                }
            }
        ).then(response => response.json())
            .then(data => {
                console.log("the data",data)
                dispatch(action.getAssetsSuccess(data))
            })
            .catch(error => {
                console.log("the data",error)
                const errorMessage = error.message
                dispatch(action.getAssetsFailure(errorMessage))
            })
    }
}

export const fetchCandleAssets = (exchange, interval, baseId, quoteId, start, end) => {
    return (dispatch) => {
        dispatch(action.getCandleRequest())
        fetch(
            `https://api.coincap.io/v2/candles?exchange=${exchange}&interval=${interval}&baseId=${baseId}&quoteId=${quoteId}&start=${start}&end=${end}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${API_KEY_COIN_CAP}`,
                    'Accept-Encoding': 'gzip',
                    'Content-Type': 'application/json'
                }
            }
        ).then(response => response.json())
            .then(data => {
                dispatch(action.getCandleSuccess(data))
            })
            .catch(error => {
                console.log(error)
                const errorMessage = error.message
                dispatch(action.getCandleFailure(errorMessage))
            })
    }
}