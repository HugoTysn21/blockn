import {combineReducers, createStore, applyMiddleware, compose} from "redux";
import assetReducer from "./Reducers/assetReducer";
import thunk from "redux-thunk";
import priceReducer from "./Reducers/priceReducer";
import iconReducer from "./Reducers/iconReducer";

// combine reducer multiple state
const rootReducers = combineReducers({
    assetReducer: assetReducer,
    priceReducer: priceReducer,
    iconReducer: iconReducer

});

export default compose(applyMiddleware(thunk))(createStore)(rootReducers)
