import AssetsComponent from "../Component/AssetsComponent";
import AssetsListItem from "../Component/AssetsListItem";
import AssetsDetail from "../Component/AssetsDetail";
import {createStackNavigator} from "@react-navigation/stack";
import HomeComponent from "../Component/HomeComponent";
import {NavigationContainer} from "@react-navigation/native";
import HomeBlockchainComponent from "../Component/HomeBlockchainComponent";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import Ionicons from 'react-native-vector-icons/Ionicons';

const AssetsStack = createStackNavigator();

export const AssetsStackNavigator = () => {
    return (
        <AssetsStack.Navigator>
            <AssetsStack.Screen options={{headerShown: false}} name="Home" component={HomeComponent}/>
            <AssetsStack.Screen options={{headerShown: false}} name="AssetsComponent" component={AssetsComponent}/>
            <AssetsStack.Screen options={{headerShown: false}} name="AssetsListItem" component={AssetsListItem}/>
            <AssetsStack.Screen options={{headerShown: false}} name="AssetsDetail" component={AssetsDetail}/>
        </AssetsStack.Navigator>
    )
}
const BlockchainStack = createStackNavigator();

export const BlockchainStackNavigator = () => {
    return (
        <BlockchainStack.Navigator>
            <BlockchainStack.Screen options={{headerShown: false}} name="Home" component={HomeBlockchainComponent}/>
        </BlockchainStack.Navigator>
    )
}

const Tab = createBottomTabNavigator();

export const TabNavigator = () => {
    return (
        <NavigationContainer>
            <Tab.Navigator screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Real-time Market') {
                        iconName = focused
                            ? 'ios-information-circle'
                            : 'ios-information-circle-outline';
                    } else if (route.name === 'Blockchain') {
                        iconName = focused ? 'ios-list' : 'ios-list';
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'gray',
            })}
            >
                <Tab.Screen name="Real-time Market" component={AssetsStackNavigator}/>
                <Tab.Screen name="Blockchain" component={BlockchainStackNavigator}/>
            </Tab.Navigator>
        </NavigationContainer>
    );
}